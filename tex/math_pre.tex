%# -*- coding: utf-8-unix -*-
%%==================================================
%% chapter02.tex for SJTU Master Thesis
%%==================================================

\chapter{数学背景知识}
\label{chap:pre}

本章主要介绍阅读本文需要的一些数学背景知识，包括概率论、图论、布尔电路。


在本文中，我们使用以下的数学符号：
\begin{itemize}
\item $\mathbb{N}$表示自然数，$\mathbb{R}$表示实数，$\mathbb{R}_+$表示正实数；
\item $e$为自然对数的底数，$\ln n$为对$n$取自然对数，$\log n$为对$n$取以$2$为底的对数，$exp(x) = e^{-x}$为以$e$为底的指数运算；
\item $[n] := \{1, 2, \dots, n\}$ 。
\end{itemize}


在理论计算机科学研究中，我们更加关心的是我们要研究的模型（图灵机上的算法、布尔电路，等等）的渐近行为，也就是当输入长度$n$足够大时的行为。
在本文中我们会大量使用表\ref{tab:math:bigO}所列的\emph{渐近符号}。
我们所比较的函数$f(n)$ 、$g(n)$均为从自然数到正实数的函数。
在大部分情况中，我们比较的函数值域范围更小，仅仅是自然数。
因为我们考虑的是渐近行为，为了行文简洁，在本文中一般会省略上取整、下取整符号。

\begin{table}[H]
  \centering
  \bicaption[tab:math:bigO]{本文使用的渐近符号}{本文使用的渐近符号}{Table}{Asymptotic Notations}
  \begin{tabular}{ccc}
  	\toprule
	符号 & 直观解释 & 形式化定义 \\ \midrule
	$f(n) = O(g(n))$ & 渐近上限 & $\exists k\in \mathbb{R}_+ ,\exists n_0 \in \mathbb{N} ,\forall n > n_0 , f(n) \leq k \cdot g(n)$\\
	$f(n) = o(g(n))$ & 渐近可忽略不计 & $\forall \epsilon\in \mathbb{R}_+, \exists n_0 \in \mathbb{N}, \forall n > n_0 , f(n) \leq \epsilon \cdot g(n)$\\
	$f(n) = \Omega(g(n))$ & 渐近下限 & $g(n) = O(f(n))$\\
	$f(n) = \omega(g(n))$ & 渐近主导 & $g(n) = o(f(n))$\\
	$f(n) = \Theta(g(n))$ & 渐近紧约束 & $f(n) = O(g(n))$且$f(n) = \Omega(g(n))$\\
	\bottomrule  
  \end{tabular}
\end{table}

对于算法的渐近运行时间，我们有如下定义：
\begin{defn}
对于一个算法$\mathrm{A}$ ，
\begin{itemize}
\item 如果存在常数$c \in \mathbb{N}$ ，使得该算法的运行时间是$O(n^c)$ ，那么我们称$\mathrm{A}$为\emph{多项式时间}算法；
\item 如果存在常数$c \in \mathbb{N} \setminus \{0\}$ ，使得该算法的运行时间是$O(n^{\log^c n})$ ，那么我们称$\mathrm{A}$为\emph{准多项式时间}算法。
\end{itemize}
\end{defn}
一个多项式$f(n)$的\emph{度数}$\mathrm{deg}(f(n))$为其系数非$0$同类项的最大度数。

\section{概率论}
概率论是数学中研究随机性的一个分支。
不管是在本文随机算法的分析过程中，还是在电路下界的证明、规约的证明中，我们都会用到概率论。


本文使用了以下概率论中的数学符号：
\begin{itemize}
\item 粗体字母$\*X$, $\*Y$, $\*x$, $\*r$等表示随机变量；
\item $\E{\*X}$为随机变量$\*X$的期望；
\item $\Var{\*X} := \E{\*X^2} - \E{\*X}^2$为随机变量$\*X$的方差；
\item 对于一个概率分布$\mathrm{D}$ ，$\*X \sim \mathrm{D}$表明随机变量$\*X$遵从概率分布$\mathrm{D}$ ；
\item 对于一个事件$A$ ，$\Pr{A}$即为其发生的概率。
\end{itemize}


随机变量的期望有以下非常好的性质：
\begin{thm}[期望的线性性]
对于随机变量$\*X$，$\*Y$，我们有
\begin{itemize}
\item $\E{\*X + \*Y} = \E{\*X} +\E{\*Y}$ ，
\item $\E{a\*X} = a\cdot \E{\*X}$ ，其中$a\in \mathbb{R}$ 。
\end{itemize}
\end{thm}


一个常见的而且在本文中也会用到的概率分布便是二项式分布$\B(n,p)$ ：
\begin{defn}
\emph{二项式分布$\B(n,p)$}是一个描述$n$次独立是/否实验的成功次数的离散概率分布， 其中每次实验的成功概率都是$p$ 。 也就是说，对于$\*X \sim \B(n,p)$ ，其概率质量函数为：
\begin{equation}
\Pr{\*X = k} = {n \choose k} p^k (1-p)^{n-k} \text{ ，}
\end{equation}
其中$k$满足$0 \leq k \leq n$ 。
\end{defn}

对于$\B(n,p)$我们有如下性质：
\begin{thm}
对于随机变量$\*X \sim \B(n,p)$ ，我们有：
\begin{align}
\E{\*X} &= np \text{ ，} \\
\Var{\*X} &= np(1-p) \text{ 。}
\end{align}
\end{thm}


本文中还使用到了超几何分布$\HG(K, N, n)$ ：
\begin{defn}
\emph{超几何分布$\HG(K, N, n)$}是一个描述由$N$个物品中不归还地抽出$n$个物品，成功抽出指定种类的物品的个数的离散概率分布，而在这$N$个物品中一共有$K$个指定种类的物品。 也就是说，对于$\*X \sim \HG(K, N, n)$ ，其概率质量函数为：
\begin{equation}
\Pr{\*X = k} = \frac{{K \choose k}{N-K \choose n-k}}{{N \choose n}}\text{ ，}
\end{equation}
其中$k$满足$0 \leq k \leq K$ ，$K$满足$K \leq N$ ，$n$满足$n \leq N$。
\end{defn}

对于$\HG(K, N, n)$我们有如下性质：
\begin{thm}
对于随机变量$\*X \sim \HG(K, N, n)$ ，我们有：
\begin{align}
\E{\*X} &= n\frac{K}{N} \text{ ，} \\
\Var{\*X} &= n\frac{K}{N}\frac{(N-K)}{N}\frac{N-n}{N-1} \text{ 。}
\end{align}
\end{thm}


因为我们更加关心的是渐近行为，我们会使用以下术语：
\begin{defn}
对于一系列可数无穷多的事件$\{A_n\}_{n \in \mathbb{N}}$:
\begin{itemize}
\item 如果存在某个常数$c > \frac{1}{2}$ ，使得对于足够大的$n$我们都有$\Pr{A_n} \geq c$ ，我们称这一系列事件\emph{以很大的概率}会发生；
\item 如果$p(n) := \Pr{A_n} = 1-o(1)$ ，我们称这一系列事件\emph{几乎可以肯定}会发生。
\end{itemize}
\end{defn}

在随机算法分析中一个常见的工具就是并集界：
\begin{thm}[Boole不等式，或称为并集界]
对于可数多个事件$A_1$, $A_2$, $\dots$, 我们有
\begin{equation}
\Pr{\bigcup_i A_i} \leq \sum_i \Pr{A_i} \text{ 。}
\end{equation}
\end{thm}


对于概率论、概率分布、随机算法分析的详细的介绍，请参考文献\citen{25,26,27}。
\subsection{集中不等式} 
在概率论中，集中不等式对于随机变量如何偏离某些数值（特别是期望值）提供了界。
经典概率论中的大数定律表明，在某些很弱的条件下随机变量之和有很大的概率很接近它们的期望。
这里的随机变量之和便是一个随即变量集中在它们的均值的很基础的例子。
而在理论计算机科学，特别是随机算法分析的研究中，我们还需要更详细的结果：
我们需要知道随机变量有多集中，也就是说有多大的概率对于某些数值有某些偏移。
本节介绍的集中不等式就对这些概率给出了上界。


本节主要介绍本文中会用到的一些集中不等式，省略了证明。
很多集中不等式有多种形式，在本节介绍的形式是在本文中会用到的那种。
对于集中不等式的全面、完整的介绍以及它们的证明，请参考文献\citen{28}。


如果我们知道一个非负随机变量的期望，我们可以使用Markov不等式：
\begin{thm}[Markov不等式]
令$\*X$为一个非负随机变量，则对于任意的$t\in \mathbb{R}_+$ ，我们有
\begin{equation}
\Pr{\*X \geq t} \leq \frac{\E{\*X}}{t} \text{ 。}
\end{equation}
\end{thm}


如果我们还知道随机变量的方差，我们可以使用Chebyshev不等式：
\begin{thm}[Chebyshev不等式]
令$\*X$为满足$\E{\*X} = \mu < \infty$ ，$\Var{\*X} = \sigma^2 \neq 0$的随机变量。
则对于任意的$t \in \mathbb{R}_+$ ，我们有
\begin{equation}
\Pr{|\*X - \mu| \ge t} \leq \frac{\sigma^2}{t^2}\text{　。}
\end{equation}
\end{thm}


对于一个遵从二项分布的随机变量，我们可以使用Chernoff界：
\begin{thm}[Chernoff界]
令随机变量$\*X \sim \B(n, p)$ ，则对于$t \in \mathbb{R}_+$：
\begin{equation}
\Pr{\*X - np \geq t} \leq \left\{
\begin{array}{ll}
\exp\tp{-\frac{t^2}{4np}} & \mbox{如果$0 < t <2np$ ，} \\
\exp\tp{-\frac{t}{2}} & \mbox{如果$t \geq 2np$ 。}
\end{array}
\right.
\end{equation}
\end{thm}


Chernoff界的一个推广版本便是Bernstein不等式：
\begin{thm}[Bernstein不等式]
令$\*X_1$, $\*X_2$, $\dots$, $\*X_n$为$n$个独立随机变量，而且存在$b \in \mathbb{R}_+$使得对于所有的$\*X_i$都有
\begin{equation}
\left| \*X_i - \E{\*X_i}\right| \leq b \text{ 。}
\end{equation}
定义随机变量$\*X$为$\sum_{i=1}^n \*X_i$ ，则对于任意的$t \in \mathbb{R}_+$ ，我们有
\begin{equation}
\Pr{\*X >\E{\*X}+t}\leq \exp\tp{-\frac{t^2}{2\cdot \Var{\*X} + \frac{2}{3}bt}} \text{　。}
\end{equation}
\end{thm}


对于随机变量之和，我们还有Kolmogorov不等式：
\begin{thm}[Kolmogorov不等式]
令$\*X_1$, $\*X_2$, $\dots$, $\*X_n$为$n$个期望为$0$的独立随机变量。对于所有的$1$到$n$中的$k$，定义$\*Y_k$为$\sum_{i=1}^k \*X_i$ ，则对于任意的$t\in \mathbb{R}_+$ ，我们有
\begin{equation}
\Pr{\max_{1\leq k \leq n} |\*Y_k| \geq t} \leq \frac{\Var{\*Y_n}}{t^2} \text{ 。}
\end{equation}
\end{thm}
\section{图}
关于图论，我们有下面的定义：
\begin{defn}
一个\emph{无向图}是一个二元组$\*G = (\*V, \*E)$	，其中$\*V$为\emph{顶点集}，$\*E \subseteq \{\{u,v\}| u,v \in \*V \wedge u \neq v\}$为\emph{边集}，无序对$\{u,v\}\in \*E$为该图的\emph{边}。
\end{defn}
注意到：
\begin{itemize}
\item $\*E$是一个无序对的集合，所以该图是无向的无重边的图；
\item $\{u,u\} \not\in \*E$ ，所以该图无自环。
\end{itemize}
本文讨论的图均是无自环无重边的无向图。 一般情况下，我们记$n:=|\*V|$ ，设$\*V = [n]$ 。 注意到我们的记号$\*G$ 、$\*V$ 、$\*E$都是粗体字母，但这并不会和前文的随机变量记法引起很大混淆：本文讨论的图都是随机生成的，只有$\*V$一般情况下是确定的$[n]$ 。


另外，我们还有如下的说法：
\begin{defn}
给定一个图$\*G = (\*V, \*E)$ ，若$\{u,v\} \in \*E$ ，我们称顶点$u$和顶点$v$\emph{邻接}，而且互为\emph{邻居}。一个顶点$v$的\emph{度数}$\mathrm{deg}(v)$定义为其邻居数目。
\end{defn}

接下来我们要定义团。
\begin{defn}
给定一个图$\*G = (\*V, \*E)$ ：
\begin{itemize}
\item 若$\*E = \{\{u,v\}| u,v \in \*V \wedge u \neq v\}$ ，我们称图$\*G$为\emph{完备图}；
\item 对于顶点集合$\*V' \subseteq \*V$ ，图$\*G$在$\*V'$上的\emph{导出子图}$\*G'$为$(\*V', \*E')$ ，其中$\*E' = \{\{u,v\}|u,v \in \*V' \wedge \{u,v\} \in \*E\}$ ；
\item 该图中的\emph{团}$\*K \subseteq \*V$是使其导出子图为完备图的顶点集合，团的大小定义为$|\*K|$；
\item 图$\*G$的\emph{团数}定义为
\begin{equation}
\omega(\*G) = \max\{|\*K| | \*K \mbox{是图$\*G$中的团}\}\text{ ；}
\end{equation}
\item 对于$k \leq n$ ，我们定义$\omega_k(\*G)$为图$\*G$中大小为$k$的团的数目；
\end{itemize}
\end{defn}
注意$\omega(\*G)$和$\omega(f(n))$中的符号$\omega$表示的是完全不同的概念。
\section{Erd\"os-R\'enyi随机图\Gnp}
我们使用的随机图模型是Erd\"os-R\' enyi随机图\Gnp ：
\begin{defn}
在\Gnp 中，通过随机地连接图中的顶点我们生成一个图$\*G$ 。每条可能的边都独立地以$p$的概率加入到图$\*G$中。换句话说，所有有$n$个顶点和$m$条边的图都有相同的概率
\begin{equation}
p^m(1-p)^{{n \choose 2} - m}\text{ 。}
\end{equation}
\end{defn}

随机图\Gnp 有很多很好的性质。比如：
\begin{thm}
若$\*G \sim \Gnp$ ，则我们有：
\begin{itemize}
\item 图中顶点$v$的度数分布是二项式分布，即
\begin{equation}
\Pr{\mathrm{deg}(v) = k} = {n-1 \choose k}p^k (1-p)^{n-1-k}\text{ ；}
\end{equation}
\item 若$p = \frac{1}{2}$ ，则几乎可以肯定$\*G$中的最大团大小约为$2 \log n$ ；
\item 更进一步地，若$p = \frac{1}{2}$ ，则存在一个函数$k: \mathbb{N} \to \mathbb{N}$ ，几乎可以肯定$\omega(\*G) = k(n)$或$k(n) + 1$ 。
\end{itemize}
\end{thm}


文献\citen{7}给出了如下算法，以很大的概率找出\Gnp 中大小约为$\log n$的团：
\begin{algorithm}[H] % 强制定位
\caption{找出\Gnp 中大小约为$\log n$的团}
\begin{algorithmic}[1] %每行显示行号
\Require 来自于\Gnp 的图$\*G = (\*V, \*E)$
\Ensure 顶点集$\*T$
\State $\*T \gets \varnothing$
\State $\*S \gets \*V$
\While{$\*S \neq \varnothing$}
	\State 随机地在$\*S$中选择顶点$v$
	\State $\*T \gets \*T \cup \{v\}$
	\State 在$\*S$删去$v$和所有不是$v$的邻居的顶点
\EndWhile
\State 输出$\*T$
\end{algorithmic}
\end{algorithm}


然而，文献\citen{7}提出的猜想至今仍未被解决：
\begin{conj}
对于任意的$\epsilon>0$ ，没有有效的方法能在\Gnp 中找出大小为$(1 + \epsilon)\log n$的团。
\end{conj}


关于\Gnp 在我们使用的设定下的性质，请参看第\ref{sec:max_clique}节。关于\Gnp 更详细的研究、讨论，请参看文献\citen{5, 6, 26}。
\section{\AC 与\ACq 电路}
在理论计算机科学中，布尔电路是一种常见的计算模型。
布尔电路是一种图，展示了如果在二进制输入上应用一系列的基本布尔操作来得到输出。
图\ref{fig:circuit}展示了一个计算两个比特的异或的布尔电路。
\afterpage{
\begin{figure}
  \centering
  \includegraphics[width=0.3\textwidth]{circuit.png}
  \bicaption[fig:circuit]{布尔电路例子}{一个计算两个比特的异或的布尔电路\footnotemark[1]}{Fig}{A circuit computing the $\mathrm{XOR}$ function}
\end{figure}
\footnotetext[1]{这个例子来自文献\citen{4}。}
}

形式化的定义如下：
\begin{defn}
对于所有的$n \in \mathbb{N}$ ，一个$n$输入、单输出的\emph{布尔电路}是一个有向无环图，并且有$n$个\emph{源}（没有入边的顶点）和$1$个\emph{汇}（没有出边的顶点）。所有的非源的顶点都被叫做\emph{门}，而且标有记号$\vee$ 、$\wedge$或$\neg$ ，分别对应逻辑操作或、与、非。其中，标有记号$\vee$或$\wedge$的顶点可以有无上界的\emph{输入端数}（入边数目），而标有记号$\neg$的顶点的输入端数是$1$ 。电路$\-C$的\emph{大小}$|\-C|$为电路中的顶点数目，\emph{深度}为由源到汇最长的路径的长度。


如果$\-C$是一个布尔电路，而$x \in \{0,1\}^n$是输入，那么$\-C$在$x$上的\emph{输出}$\-C(x)$用以下方式定义：
\begin{itemize}
\item 对于$\-C$中的每个顶点$v$ ，我们用以下方式得到其数值$\mathrm{val}(v)$：
如果$v$是第$i$个源，那么$\mathrm{val}(v) = x_i$ ，否则$\mathrm{val}(v)$为递归地应用顶点$v$相对应的逻辑操作在$v$的入边连接的顶点的数值上得到的结果；
\item $\-C(x)$定义为汇的数值。
\end{itemize}
\end{defn}


在\ACq 电路中，我们还允许使用以下的门，其中$q$为质数：
\begin{equation}
\mathrm{MOD}_q(x_1, \dots, x_m) = \left\{
\begin{array}{ll}
1 & \text{如果$\sum_{i=1}^m x_i \mod q = 0$ ，}\\
0 & \text{否则。}
\end{array}\right.
\end{equation}
注意到，在文献\citen{4}中，该电路一般被记为$\mathbf{AC}^0[p]$ ；在本文中，为了防止$p$与边概率$p(n)$混淆，一律记为\ACq 。


下面我们就可以定义\AC 和\ACq 电路：
\begin{defn}
\begin{itemize}
\item 令$T: \mathbb{N} \to \mathbb{N}$为一个函数，则一个\emph{$T(n)$大小的布尔电路族}是一个布尔电路序列$\{\-C_n\}_{n \in \mathbb{N}}$ ，其中$\-C_n$是$n$输入、单输出，而且对于足够大的$n$我们有$|\-C_n| \leq T(n)$ ；
\item 一个\emph{\AC 电路族}是一个多项式大小、常数深度的布尔电路序列，其中每个电路可以使用输入端数无上界的$\vee$和$\wedge$门，以及$\neg$门；
\item 对于质数$q$ ，一个\emph{\ACq 电路族}是一个多项式大小、常数深度的布尔电路序列，其中每个电路可以使用输入端数无上界的$\vee$ 、$\wedge$和$\mathrm{MOD}_q$门，以及$\neg$门。
\end{itemize}
\end{defn}

\begin{defn}
对于一个问题$L$ ，
\begin{itemize}
\item 如果存在一个\AC 电路族$\{\-C_n\}_{n \in \mathbb{N}}$使得对于所有的输入$x \in \{0,1\}^n$都有$x \in L \Leftrightarrow \-C_n(x) = 1$ ，则称$L \in \AC$ ；
\item 如果存在一个\ACq 电路族$\{\-C_n\}_{n \in \mathbb{N}}$使得对于所有的输入$x \in \{0,1\}^n$都有$x \in L \Leftrightarrow \-C_n(x) = 1$ ，则称$L \in \ACq$ ；
\end{itemize}
\end{defn}

关于布尔电路、电路复杂性的深入讨论，请参考文献\citen{4,29,30}。